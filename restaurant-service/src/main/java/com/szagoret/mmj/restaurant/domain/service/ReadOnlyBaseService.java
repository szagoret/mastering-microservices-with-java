package com.szagoret.mmj.restaurant.domain.service;

import com.szagoret.mmj.restaurant.domain.repository.Repository;

/**
 *
 * @author Sergiu Zagoret
 * @param <TE>
 * @param <T>
 */
public abstract class ReadOnlyBaseService<TE, T> {

    private Repository<TE, T> repository;

    ReadOnlyBaseService(Repository<TE, T> repository) {
        this.repository = repository;
    }
}
