package com.szagoret.mmj.eureka.service;

/**
 *
 * @author Sergiu Zagoret
 */
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 *
 * @author Sergiu Zagoret
 */
@SpringBootApplication
@EnableEurekaServer
public class App {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
