package com.szagoret.mmj.user.domain.service;

import com.szagoret.mmj.user.domain.repository.Repository;

/**
 *
 * @author Sergiu Zagoret
 * @param <TE>
 * @param <T>
 */
public abstract class ReadOnlyBaseService<TE, T> {

    private Repository<TE, T> repository;

    ReadOnlyBaseService(Repository<TE, T> repository) {
        this.repository = repository;
    }
}
