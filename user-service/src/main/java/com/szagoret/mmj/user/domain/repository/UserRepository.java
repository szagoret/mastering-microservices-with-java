package com.szagoret.mmj.user.domain.repository;

import java.util.Collection;

/**
 *
 * @author Sergiu Zagoret
 * @param <User>
 * @param <String>
 */
public interface UserRepository<Booking, String> extends Repository<Booking, String> {

    /**
     *
     * @param name
     * @return
     */
    boolean containsName(String name);

    /**
     *
     * @param name
     * @return
     * @throws Exception
     */
    public Collection<Booking> findByName(String name) throws Exception;
}
