package com.szagoret.mmj.user.domain.repository;

import com.szagoret.mmj.user.domain.model.entity.Entity;
import java.util.Collection;

/**
 *
 * @author Sergiu Zagoret
 * @param <TE>
 * @param <T>
 */
public interface ReadOnlyRepository<TE, T> {

    //long Count;
    /**
     *
     * @param id
     * @return
     */
    boolean contains(T id);

    /**
     *
     * @param id
     * @return
     */
    Entity get(T id);

    /**
     *
     * @return
     */
    Collection<TE> getAll();
}
